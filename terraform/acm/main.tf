resource "aws_acm_certificate" "this" {
  private_key               = file("server.key")
  certificate_body          = file("server.crt")
}