####################################################### EKS ################################################################
cluster_version                 = "1.27"
cluster_enabled_log_types       = []
cluster_security_groups         = []
cluster_endpoint_private_access = false
cluster_endpoint_public_access  = true
cluster_public_access_cidrs     = []
enable_irsa                     = true
openid_connect_audiences        = []
custom_oidc_thumbprints         = []
create_iam_role                 = true
iam_role_arn                    = null
nodes_iam_role_arn              = null
node_group_name                 = "nodes-eks-dev"
instance_types                  = ["t3.medium"]
ami_type                        = "AL2_x86_64"
release_version                 = "1.27.3-20230728"
desired_size                    = 2
max_size                        = 2
min_size                        = 0
enable_remote_access            = false
ec2_ssh_key                     = null
cluster_tags                    = {}
node_tags                       = {}
tags = {
  "ManagedBy" = "Terraform"
  "CI"        = "Gilab"
}