terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.2.0"
    }
  }

  backend "s3" {
    bucket = "challengue-bpi-0893"
    key    = "eks/{{ENVIRONMENT}}/terraform.tfstate"
    region = "us-east-2"
  }

  required_version = "> 1.0.0"
}

provider "aws" {
  region = "us-east-2"
}

########################## REMOTE STATES ######################################
data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = "challengue-bpi-0893"
    key    = "vpc/{{ENVIRONMENT}}/terraform.tfstate"
    region = "us-east-2"
  }
}
