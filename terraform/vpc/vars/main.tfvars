####################################################### VPC ################################################################
networks = {
  cidr_block      = "172.96.0.0/24"
  public_subnets = ["172.96.0.0/26", "172.96.0.64/26"]
  public_azs     = ["us-east-2a", "us-east-2b"]
  create_igw      = true
}
name = "vpc-main"
tags = {
  "ManagedBy" = "Terraform"
  "CI"        = "Gilab"
}