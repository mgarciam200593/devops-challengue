####################################################### VPC ################################################################
module "vpc" {
  source = "git::https://gitlab.com/mgarciam200593/vpc?ref=tags/v1.0.2"

  name     = var.name
  networks = var.networks
  tags = merge(
    var.tags,
    { "Environment" = var.environment }
  )
}