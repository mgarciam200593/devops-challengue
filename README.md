# EJERCICIO DEVOPS BP

## Aplicación

El proyecto contiene la aplicación **demo-devops-java** desarrollada en Java Spring Boot, la cual es una API que agrega regista usuarios mediante su DNI y Nombre (POST) y posteriormente se consulta la totalidad de usuarios o uno determinado por ID (GET).

## Arquitectura

La API es desplegada en Kubernetes bajo infraestructura de AWS, la cual es desplegada utilizando Terraform.\
El diagrama de la arquitectura a continuación:\
![image info](./Images/architecture/AWS_arch.png)


## Infraestructura

La infraestructura es desplegada utilizando Terraform y se despliegan los siguientes servicios de AWS:
- VPC
- ACM (Certificate Manager)
- EKS

Los recursos de [VPC](https://gitlab.com/mgarciam200593/vpc) y [EKS](https://gitlab.com/mgarciam200593/eks) son creados utilizando módulos creados para el presente proyecto.


## Docker

La API es dockerizada utilizando el archivo Dockerfile que se encuentra en la carpeta **java/**, donde también se encuentran el código y propiedades que componen al aplicación.

## Kubernetes

Los manifiestos para la creación de recursos de Kubernetes se encuentran en la carpeta **kubernetes/**, en la cual se encuentran dos subdirectorios:
- nginx: contiene los manifiestos para la instalación de Nginx Ingress Controller
- app: contiene los manifiestos para el despliegue de la API utilizando el recurso Ingress

A continuación se presenta el diagrama a alto nivel de los recursos que se despliegan en Kuberntes:\
![image info](./Images/architecture/NGINX.png)

Las peticiones hacia la aplicación ingresan por un balanceador público en la nube de AWS, este balanceador (NLB) re-dirige el tráfico hacia los pods de Nginx Ingress Controller y, finalmente, Nginx re-digire el tráfico hacia los pods si es que la petición cumple con las reglas establecidas en su archivo de configuración (e.g. se configura el acceso al path /api/users en Nginx, por ende sólo las peticiones hacia dicho path recibirán respuesta, caso contrario se recibe 404).

## Pipeline

El pipeline de Gitlab se encarga de la creación de la infraestructura así como de la contenerización de la aplicación y su despliegue en Kubernetes.\
El despligue se realiza en 3 ambientes: dev, staging y main.\
El pipeline contiene 12 stages, los cuales se explican a continuación:

- openssl: genera el certificado auto-firmado que se utilizará en el balanceador de AWS, con el nombre de dominio dependiendo de la rama en la que se ejecuta el pipeline (e.g. en dev el dominio es "dev.devops.com" y en staging es "staging.devops.com")
- unit_test: ejecuta los tests unitarios de al aplicación Java
- sonar: ejecuta el escaneo de Sonarqube sobre la aplicación
![image info](./Images/sonarqube/sonar.png)
- create_repo: crea un repositorio privado ECR para almacenar las imágenes de los contenedores, se crea un repositorio por cada ambiente utilizando AWS CLI
- build_maven: se ejecuta el comando ```mvn clean package``` para generar el archivo .jar de la aplicación Java como un artefacto
- build_image: se realiza el ```docker build``` y ```docker push``` de la aplicación hacia el repositorio ECR, tomando el archivo .jar del stage anterior para construir la imagen
- acm_create: se importa el certificado en ACM utilizando los archivos ```server.crt``` y ```server.key``` generados en el stage **openssl**, utilizando Terraform
- vpc_create: se crea la infraestructura de VPC utilizando Terraform
- eks_crate: se crea la infraestructura de EKS utilizando Terraform
- nginx: se crean los recursos de Kubernetes para desplegar Nginx Ingress Controller
- deploy_app: se crean los recursos de Kubernetes para desplegar la API de Java Spring Boot
- cleanup: stage que se dispara por acción manual, que destruye los recursos de Kubernetes y la infraestructura de AWS utilizando Terraform